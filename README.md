# effigies.gitlab.io

This is the source for a personal website: https://effigies.gitlab.io

## License

Unless otherwise stated, all textual content is released under
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/), and
all code snippets are released under the
[MIT License](https://mit-license.org/).
